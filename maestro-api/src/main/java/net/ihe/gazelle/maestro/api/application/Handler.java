package net.ihe.gazelle.maestro.api.application;

/**
 * An Handler is an adapter that interact with I/O, web, UI or any external peripheral during test
 * execution. They are used by {@link StepRunner}.
 *
 */
public interface Handler {

    /** Check is available.
     * @return true if the handler is available, false if it is not.
     */
    boolean isAvailable();
}
