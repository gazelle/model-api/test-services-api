package net.ihe.gazelle.maestro.api.domain.step.assertion;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class AssertRegex extends AbstractAssert {

    public static final String REGEX = "regex";

    public static final String CONTENT = "content";

    public static final String GROUP = "group";

    public static final String FLAGS = "FLAGS";

    @Override
    public Set<String> getRequiredPropertyKeys() {
        return new HashSet<>(Arrays.asList(
                REGEX
        ));
    }
}
