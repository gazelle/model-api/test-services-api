package net.ihe.gazelle.maestro.api.application;

import net.ihe.gazelle.maestro.api.domain.Step;

public interface StepRunner<S extends Step> {

   Class<? extends Handler>[] getRequiredHandlers();

   void setHandlers(Handler... handlers) ;

   void runStep(final RunningStep runningStep, Runnable maestroCallback);

}
