package net.ihe.gazelle.maestro.api.domain;

import io.github.ceoche.bvalid.BasicRules;
import io.github.ceoche.bvalid.BusinessObject;
import io.github.ceoche.bvalid.BusinessRule;

import java.util.HashMap;
import java.util.Map;

@BusinessObject(name = "step-run")
public class StepRun {

   private String id;
   private StepResult result;
   private final Map<String, String> properties = new HashMap<>();

   public StepRun() {
   }

   public StepRun(String id, StepResult result, Map<String, String> properties) {
      this.id = id;
      this.result = result;
      this.properties.putAll(properties);
   }

   public String getId() {
      return id;
   }

   public StepRun setId(String id) {
      this.id = id;
      return this;
   }

   public StepResult getResult() {
      return result;
   }

   public StepRun setResult(StepResult result) {
      this.result = result;
      return this;
   }

   public String getProperty(String key) {
      return properties.get(key);
   }

   public StepRun setProperty(String key, String value) {
      properties.put(key, value);
      return this;
   }

   @BusinessRule(description = "Step run must refer to the step by its id.")
   public boolean isStepRunIdDefined() {
      return BasicRules.isDefined(id);
   }

   @BusinessRule(description = "Referred step id must not have chars '${}().' nor spaces.")
   public boolean hasIdRightChars() {
      return Step.hasIdRightChars(id);
   }

   @BusinessRule(description = "Step run must have a result.")
   public boolean isResultDefined() {
      return BasicRules.isDefined(result);
   }
}
