package net.ihe.gazelle.maestro.api.domain;

import io.github.ceoche.bvalid.BasicRules;
import io.github.ceoche.bvalid.BusinessMember;
import io.github.ceoche.bvalid.BusinessObject;
import io.github.ceoche.bvalid.BusinessRule;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@BusinessObject(name = "test")
public class Test {

   private String name;
   private final List<Step> steps = new ArrayList<>();

   public Test(){}

   public Test(String name) {
      this.name = name;
   }

   public String getName() {
      return name;
   }

   public Test setName(String name) {
      this.name = name;
      return this;
   }

   public Step getStep(String stepId) {
      return steps.stream().filter(s -> s.getId().equals(stepId))
            .findFirst().orElseThrow(NoSuchElementException::new);
   }

   public Step getStep(int index) {
      return steps.get(index);
   }

   public Test addStep(Step step) {
      steps.add(step);
      return this;
   }

   public Test addSteps(List<Step> steps) {
      this.steps.addAll(steps);
      return this;
   }

   @BusinessMember(name = "steps")
   @SuppressWarnings("unused")
   public List<Step> getSteps() {
      return new ArrayList<>(steps);
   }

   @BusinessRule(description = "Test must have a name")
   @SuppressWarnings("unused")
   public boolean isNameValid() {
      return BasicRules.isDefined(name);
   }

   @BusinessRule(description = "Test must have at least one defined step")
   @SuppressWarnings("unused")
   public boolean hasOneOrMoreDefinedSteps() {
      return BasicRules.hasOneOrMoreDefinedElements(steps);
   }
}
