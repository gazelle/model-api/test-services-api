package net.ihe.gazelle.maestro.api.domain;

import java.util.function.Consumer;

/**
 * Gazelle test runner
 */
public interface Maestro {

   /**
    * Run a test
    *
    * @param test     test to run
    * @param callback callback to invoke to return the {@link TestRun} once execution is over.
    *
    * @throws InvalidTestException  if the given test is invalid
    * @throws PropertyLoopException if variable properties in steps are making a cyclic reference.
    */
   void run(Test test, Consumer<ExecutionStatus> statusCallback , Consumer<TestRun> callback);

}
