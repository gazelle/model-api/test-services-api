package net.ihe.gazelle.maestro.api.application.handler.ui;

import net.ihe.gazelle.maestro.api.application.Handler;

/**
 * Test Handler dedicated to interacting with a human test operator.
 */
// FIXME update method signatures with asynchronous callbacks
public interface UIHandler extends Handler {

   /**
    * Print a message to the test operator.
    *
    * @param message to display to the operator
    */
   void print(String message);

   /**
    * Present a text input to the test operator labeled with the given message. The method will
    * block until the operator inputs a value.
    *
    * @param inputLabel Label of the input to display to the operator
    * @return the value entered by the operator
    */
   String scan(String inputLabel);

}
