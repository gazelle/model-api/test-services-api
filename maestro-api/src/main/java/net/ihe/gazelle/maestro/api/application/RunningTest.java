package net.ihe.gazelle.maestro.api.application;

import net.ihe.gazelle.maestro.api.domain.Step;
import net.ihe.gazelle.maestro.api.domain.Test;
import net.ihe.gazelle.maestro.api.domain.TestRun;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RunningTest {

   private final Test test;
   private final Iterator<Step> stepIterator;
   private final LinkedHashMap<String, RunningStep> runningSteps = new LinkedHashMap<>();
   private final RunningStepFactory runningStepFactory;

   public RunningTest(Test test, RunningStepFactory runningStepFactory) {
      this.test = test;
      this.runningStepFactory = runningStepFactory;
      stepIterator = test.getSteps().iterator();
   }

   public List<RunningStep> getRunningSteps() {
      return new ArrayList<>(runningSteps.values());
   }

   public RunningStep getRunningStep(String stepName) {
      return runningSteps.get(stepName);
   }

   public boolean hasStepToRun() {
      return stepIterator.hasNext();
   }

   public RunningStep nextRunningStep() {
      RunningStep runningStep = runningStepFactory.getInstance(stepIterator.next(), this);
      runningSteps.put(runningStep.getId(), runningStep);
      return runningStep;
   }

   public TestRun toTestRun() {
      TestRun testRun = new TestRun(test.getName());
      testRun.addStepRuns(
            runningSteps.values().stream().map(RunningStep::toStepRun).collect(Collectors.toList())
      );
      return testRun;
   }

   public Stream<Step> stepsStream() {
      return test.getSteps().stream();
   }
}
