package net.ihe.gazelle.maestro.api.application;

import net.ihe.gazelle.maestro.api.domain.Step;
import net.ihe.gazelle.maestro.api.domain.StepResult;
import net.ihe.gazelle.maestro.api.domain.StepRun;

import java.util.Map;
import java.util.Set;

public abstract class RunningStep {

   protected final Step step;
   protected final RunningTest runningTest;
   private StepResult result = StepResult.ERROR;

   protected RunningStep(Step step, RunningTest runningTest) {
      this.step = step;
      this.runningTest = runningTest;
   }

   public String getId() {
      return step.getId();
   }

   public Set<String> getRequiredPropertyKeys() {
      return step.getRequiredPropertyKeys();
   }

   public abstract String getProperty(String key);

   public abstract RunningStep setProperty(String key, String value);

   public abstract Map<String, String> getProperties();

   public StepResult getResult() {
      return result;
   }

   public RunningStep setResult(StepResult stepResult) {
      this.result = stepResult;
      return this;
   }

   public Step getStep() {
      return step;
   }

   public StepRun toStepRun() {
      return new StepRun(getId(), getResult(), getProperties());
   }

}
