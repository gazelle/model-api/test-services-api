package net.ihe.gazelle.maestro.api.domain;

public enum ExecutionStatus {

    PENDING,
    RUNNING,
    DONE;
}
