package net.ihe.gazelle.maestro.api.application;

import net.ihe.gazelle.maestro.api.domain.Step;

public interface RunningStepFactory {

   RunningStep getInstance(Step step, RunningTest runningTest);

}
