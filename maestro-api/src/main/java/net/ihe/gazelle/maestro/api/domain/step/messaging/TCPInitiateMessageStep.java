package net.ihe.gazelle.maestro.api.domain.step.messaging;

import net.ihe.gazelle.maestro.api.domain.Step;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class TCPInitiateMessageStep extends Step {

   public static final String REQUEST_B64 = "requestB64";
   public static final String TO_HOST = "toHost";
   public static final String TO_PORT = "toPort";
   public static final String RESPONSE_B64 = "responseB64";

   public TCPInitiateMessageStep() {
   }

   public TCPInitiateMessageStep(String id) {
      super(id);
   }

   @Override
   public Set<String> getRequiredPropertyKeys() {
      return new HashSet<>(Arrays.asList(
            REQUEST_B64,
            TO_HOST,
            TO_PORT
      ));
   }
}
