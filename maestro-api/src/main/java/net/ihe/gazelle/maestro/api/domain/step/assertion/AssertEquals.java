package net.ihe.gazelle.maestro.api.domain.step.assertion;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class AssertEquals extends AbstractAssert {

    public static final String EXPECTED = "expected";

    public static final String ACTUAL = "actual";

    @Override
    public Set<String> getRequiredPropertyKeys() {
        return new HashSet<>(Arrays.asList(
                EXPECTED,
                ACTUAL
        ));
    }
}
