package net.ihe.gazelle.maestro.api.interlay;


import net.ihe.gazelle.lang.ReflectionOperations;

public interface MaestroTopics  {

   public static final String MAESTRO_CALLBACK = "maestro-callback";

   public static final String[] TOPICS = (new ReflectionOperations())
           .getStringConstantValuesReflection(MaestroTopics.class);





}

