package net.ihe.gazelle.maestro.api.domain.step.ui;

import net.ihe.gazelle.maestro.api.domain.Step;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class InputUIStep extends Step {

   public static final String INPUT_LABEL = "inputLabel";
   public static final String INPUT_VALUE = "inputValue";

   public InputUIStep() {
   }

   public InputUIStep(String id) {
      super(id);
   }

   @Override
   public Set<String> getRequiredPropertyKeys() {
      return new HashSet<>(Arrays.asList(INPUT_LABEL));
   }
}
