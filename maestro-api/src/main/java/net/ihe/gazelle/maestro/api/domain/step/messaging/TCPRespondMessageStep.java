package net.ihe.gazelle.maestro.api.domain.step.messaging;

import net.ihe.gazelle.maestro.api.domain.Step;

import java.util.HashSet;
import java.util.Set;

public class TCPRespondMessageStep extends Step {

   public static final String LISTEN_FROM_HOST = "listenFromHost";
   // FIXME should establish binding in an allowed port-range.
   public static final String BINDING_PORT = "bindingPort";
   public static final String RECEIVED_REQUEST_B64 = "receivedRequest";

   public TCPRespondMessageStep() {
   }

   public TCPRespondMessageStep(String id) {
      super(id);
   }

   @Override
   public Set<String> getRequiredPropertyKeys() {
      return new HashSet<>();
   }
}
