package net.ihe.gazelle.maestro.api.domain;

public enum StepResult {

   DONE,
   ERROR,
   PASSED,
   FAILED;


}

// Priority order where we take max at TestResult computation

// ERROR:  If we have at least 1 error => 1000
// FAILED: If we have at least 1 failed & 0 error => 100
// PASSED: If we have at least 1 passed & 0 error  & 0 failed => 10
// DONE:   If we have at least 1 DONE   & 0 passed & 0 failed & 0 error => 1