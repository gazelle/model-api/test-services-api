package net.ihe.gazelle.maestro.api.application;

import java.util.function.Consumer;

public interface CallbacksManager<T> {

    Consumer<T> getCallback(String key);

    String addCallback(Consumer<T> callback);

}
