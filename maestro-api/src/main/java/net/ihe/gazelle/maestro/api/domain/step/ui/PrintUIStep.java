package net.ihe.gazelle.maestro.api.domain.step.ui;

import net.ihe.gazelle.maestro.api.domain.Step;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class PrintUIStep extends Step {

   public static final String DISPLAY_TEXT = "displayText";

   public PrintUIStep() {
   }

   public PrintUIStep(String id) {
      super(id);
   }

   @Override
   public Set<String> getRequiredPropertyKeys() {
      return new HashSet<>(Arrays.asList(DISPLAY_TEXT));
   }
}
