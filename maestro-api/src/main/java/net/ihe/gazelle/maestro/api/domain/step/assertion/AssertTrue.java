package net.ihe.gazelle.maestro.api.domain.step.assertion;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class AssertTrue extends AbstractAssert {

    public static final String CONDITION = "condition";

    @Override
    public Set<String> getRequiredPropertyKeys() {
        return new HashSet<>(Arrays.asList(
                CONDITION
        ));
    }
}
