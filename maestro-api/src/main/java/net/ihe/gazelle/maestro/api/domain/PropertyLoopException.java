package net.ihe.gazelle.maestro.api.domain;

public class PropertyLoopException extends RuntimeException {

   public PropertyLoopException(String message) {
      super(message);
   }
}
