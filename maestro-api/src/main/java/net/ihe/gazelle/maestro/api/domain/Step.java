package net.ihe.gazelle.maestro.api.domain;

import io.github.ceoche.bvalid.BasicRules;
import io.github.ceoche.bvalid.BusinessObject;
import io.github.ceoche.bvalid.BusinessRule;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@BusinessObject(name = "step")
public abstract class Step {

   public static final String ERROR = "error";

   private final Map<String, String> properties = new HashMap<>();
   private String id;

   protected Step() {
   }

   protected Step(String id) {
      this.id = id;
   }

   public String getId() {
      return id;
   }

   public Step setId(String id) {
      this.id = id;
      return this;
   }

   public String getProperty(String key) {
      return properties.get(key);
   }

   public Step setProperty(String key, String value) {
      properties.put(key, value);
      return this;
   }

   public Map<String, String> getProperties() {
      return new HashMap<>(properties);
   }

   public abstract Set<String> getRequiredPropertyKeys();

   @BusinessRule(description = "Step must have an id.")
   @SuppressWarnings("unused")
   public boolean isIdDefined() {
      return BasicRules.isDefined(id);
   }

   @BusinessRule(description = "Step id must not use chars '${}().' nor spaces.")
   @SuppressWarnings("unused")
   public boolean hasIdRightChars() {
      return hasIdRightChars(id);
   }

   public static boolean hasIdRightChars(String stepId) {
      return BasicRules.matches("^[^\\s\\.{}\\$\\(\\)]+$", stepId);
   }

   @BusinessRule(description = "Required properties of a step must be provided.")
   @SuppressWarnings("unused")
   public boolean areRequiredPropertiesProvided() {
      return getRequiredPropertyKeys().stream()
            .map(this::getProperty)
            .allMatch(BasicRules::isDefined);
   }
}
