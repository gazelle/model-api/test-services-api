package net.ihe.gazelle.maestro.api.domain.step.messaging;

import net.ihe.gazelle.maestro.api.domain.Step;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class HTTPSendMessageStep extends Step {

    public static final String TO_HOST = "toHost";
    public static final String TO_PORT = "toPort";
    public static final String HTTP_VERSION = "httpVersion";
    public static final String METHOD = "method";
    public static final String RESOURCE = "resource";
    public static final String QUERY_PARAMS = "queryParams";
    public static final String HEADERS = "headers";
    public static final String REQUEST_B64 = "requestB64";
    public static final String RESPONSE_B64 = "responseB64";
    public static final String RESPONSE_STATUS = "responseStatus";


    @Override
    public Set<String> getRequiredPropertyKeys() {
        return new HashSet<>(Arrays.asList(
                TO_HOST,
                TO_PORT,
                METHOD,
                RESOURCE
        ));
    }
}
