package net.ihe.gazelle.maestro.api.domain;

import io.github.ceoche.bvalid.BasicRules;
import io.github.ceoche.bvalid.BusinessMember;
import io.github.ceoche.bvalid.BusinessObject;
import io.github.ceoche.bvalid.BusinessRule;
import net.ihe.gazelle.lang.BidMap;

import java.util.*;

@BusinessObject(name = "test-run")
public class TestRun {

   private static final BidMap<StepResult, Integer> stepResultsWeights = BidMap.of(
        new BidMap.Entry<>(StepResult.DONE, 0),
        new BidMap.Entry<>(StepResult.PASSED, 1),
        new BidMap.Entry<>(StepResult.FAILED, 2),
        new BidMap.Entry<>(StepResult.ERROR, 3)
   );

   private final String testName;
   private final LinkedHashMap<String, StepRun> steps = new LinkedHashMap<>();

   public TestRun(String testName) {
      this.testName = testName;
   }

   public String getTestName() {
      return testName;
   }

   public StepRun getStepRun(int index) {
      return steps.values().toArray(new StepRun[]{})[index];
   }

   public StepRun getStepRun(String stepId) {
      return steps.get(stepId);
   }

   @BusinessMember(name = "step-runs")
   public List<StepRun> getStepRuns() {
      return new ArrayList<>(steps.values());
   }

   public TestRun addStepRun(StepRun stepRun) {
      steps.put(stepRun.getId(), stepRun);
      return this;
   }

   public TestRun addStepRuns(List<StepRun> stepRuns) {
      stepRuns.forEach(this::addStepRun);
      return this;
   }

   public TestResult getResult() {
      return getTestResultFromWeight(
            steps.values().stream()
            .map(e -> stepResultsWeights.get(e.getResult()))
            .reduce(this::keepHeaviestResult)
            .orElse(stepResultsWeights.get(StepResult.ERROR))
      );
   }

   @BusinessRule(description = "Test run must refer to the test by its name.")
   public boolean isNameValid() {
      return BasicRules.isDefined(testName);
   }

   @BusinessRule(description = "Test run must have at least one defined step run.")
   public boolean hasOneOrMoreDefinedStepRuns() {
      return BasicRules.hasOneOrMoreDefinedElements(steps.entrySet())
            && BasicRules.hasOneOrMoreDefinedElements(getStepRuns());
   }

   private Integer keepHeaviestResult(Integer prevResult, Integer currentResult) {
      return currentResult > prevResult ? currentResult : prevResult ;
   }

   private TestResult getTestResultFromWeight(Integer value) {
      return TestResult.valueOf(stepResultsWeights.getKey(value).name());
   }

}
