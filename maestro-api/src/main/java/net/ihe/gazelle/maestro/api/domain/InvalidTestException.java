package net.ihe.gazelle.maestro.api.domain;

public class InvalidTestException extends RuntimeException {

   public InvalidTestException(String message) {
      super(message);
   }

}
