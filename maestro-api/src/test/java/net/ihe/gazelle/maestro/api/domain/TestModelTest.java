package net.ihe.gazelle.maestro.api.domain;

import net.ihe.gazelle.maestro.api.domain.step.ui.InputUIStep;
import net.ihe.gazelle.maestro.api.domain.step.ui.PrintUIStep;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class TestModelTest {

   @org.junit.jupiter.api.Test
   void testWithoutNameError() {
      Test test = new Test().addStep(
            new PrintUIStep("step-1")
                  .setProperty(PrintUIStep.DISPLAY_TEXT, "Hello world!")
      );
      new ValidationAdapter(test)
            .assertIsInvalid()
            .assertRuleFailure("Test must have a name");
   }

   @org.junit.jupiter.api.Test
   void testWithoutStepsError() {
      Test test = new Test().setName("test-name");
      new ValidationAdapter(test)
            .assertIsInvalid()
            .assertRuleFailure("Test must have at least one defined step");
   }

   @org.junit.jupiter.api.Test
   void testWithNullStepError() {
      Test test = new Test().setName("test-name").addStep(null);
      new ValidationAdapter(test)
            .assertIsInvalid()
            .assertRuleFailure("Test must have at least one defined step");
   }

   @org.junit.jupiter.api.Test
   void testWithStepNoIdError() {
      Test test = new Test("name").addStep(
            new PrintUIStep()
                  .setProperty(PrintUIStep.DISPLAY_TEXT, "Hello world!")
      );
      new ValidationAdapter(test)
            .assertIsInvalid()
            .assertRuleFailure("Step must have an id.");
   }

   @ParameterizedTest
   @ValueSource(strings = {"idWith space", "idWith.dot", "idWith{curlyBracket", "idWithCurlyBracket}",
         "idWith$", "idWith(bracket", "idWithBracket)"})
   void testWithStepInvalidIdError(String stepId) {
      Test test = new Test("Name").addStep(
            new PrintUIStep(stepId)
                  .setProperty(PrintUIStep.DISPLAY_TEXT, "Hello world!")
      );
      new ValidationAdapter(test)
            .assertIsInvalid()
            .assertRuleFailure("Step id must not use chars '${}().' nor spaces.");
   }

   @org.junit.jupiter.api.Test
   void testWithStepMissingPropertyError() {
      Test test = new Test("Name").addStep(
            new PrintUIStep("stepId")
      );
      new ValidationAdapter(test)
            .assertIsInvalid()
            .assertRuleFailure("Required properties of a step must be provided.");
   }

   @org.junit.jupiter.api.Test
   void testValidTest() {
      Test test = new Test("Valid test").addStep(
            new PrintUIStep()
                  .setId("uiStep")
                  .setProperty(PrintUIStep.DISPLAY_TEXT, "Hello world!")
      );
      new ValidationAdapter(test).assertIsValid();
      assertEquals("Valid test", test.getName());
   }

   @org.junit.jupiter.api.Test
   void testManySteps() {
      Test test = buildTest("Valid test",
            new PrintUIStep("hello")
                  .setProperty(PrintUIStep.DISPLAY_TEXT, "Welcome!"),
            new InputUIStep("inputName")
                  .setProperty(InputUIStep.INPUT_LABEL, "What is your name ?"),
            new PrintUIStep("greetings")
                  .setProperty(PrintUIStep.DISPLAY_TEXT, "Hello ${inputName.inputValue}!")
      );

      new ValidationAdapter(test).assertIsValid();

      assertEquals(3, test.getSteps().size(), "All steps must be kept.");

      assertEquals("hello", test.getStep(0).getId(),
            "Steps must be kept in the same order");
      assertEquals("inputName", test.getStep(1).getId(),
            "Steps must be kept in the same order");
      assertEquals("greetings", test.getStep(2).getId(),
            "Steps must be kept in the same order");

      assertEquals(test.getStep(0), test.getStep("hello"),
            "Steps can be found by name");
      assertEquals(test.getStep(1), test.getStep("inputName"),
            "Steps can be found by name");
      assertEquals(test.getStep(2), test.getStep("greetings"),
            "Steps can be found by name");
   }

   private Test buildTest(String testName, Step... steps) {
      return new Test(testName).addSteps(List.of(steps));
   }

}
