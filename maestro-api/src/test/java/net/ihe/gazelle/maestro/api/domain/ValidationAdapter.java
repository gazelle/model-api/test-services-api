package net.ihe.gazelle.maestro.api.domain;

import io.github.ceoche.bvalid.BValidator;
import io.github.ceoche.bvalid.ObjectResult;
import io.github.ceoche.bvalid.RuleResult;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ValidationAdapter {

   private final ObjectResult result;

   public ValidationAdapter(Object subject) {
      result = new BValidator().validate(subject);
   }

   public ValidationAdapter assertIsValid() {
      assertTrue(result.isValid(), "result should be valid");
      return this;
   }

   public ValidationAdapter assertIsInvalid() {
      assertFalse(result.isValid(), "result should be invalid");
      return this;
   }

   public ValidationAdapter assertRuleFailure(String expected) {
      List<RuleResult> failedRules = aggregateRuleFailures(result);
      assertFalse(failedRules.isEmpty(), "Result should have at least one rule failure.");
      failedRules.stream()
            .filter(ruleResult -> expected.equals(ruleResult.getDescription()))
            .findAny().orElseThrow(
                  () -> new AssertionError("Did not find any failed rule with expexcted description: " + expected));
      return this;
   }

   private List<RuleResult> aggregateRuleFailures(ObjectResult objectResult) {
      List<RuleResult> failedRules = objectResult.getRuleFailures();
      objectResult.getMemberResults().forEach(
            memberResult -> failedRules.addAll(aggregateRuleFailures(memberResult))
      );
      return failedRules;
   }

}
