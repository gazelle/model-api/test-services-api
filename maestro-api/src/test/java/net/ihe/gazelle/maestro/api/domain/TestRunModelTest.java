package net.ihe.gazelle.maestro.api.domain;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TestRunModelTest {

   @Test
   void testNoNameTestRunError() {
      TestRun testRun = new TestRun(null);
      new ValidationAdapter(testRun)
            .assertIsInvalid()
            .assertRuleFailure("Test run must refer to the test by its name.");
   }

   @Test
   void testNoStepRunError() {
      TestRun testRun = new TestRun("Test 1");
      new ValidationAdapter(testRun)
            .assertIsInvalid()
            .assertRuleFailure("Test run must have at least one defined step run.");
      assertEquals(TestResult.ERROR, testRun.getResult(),
            "A test with no step-runs must have an ERROR result.");
   }

   @ParameterizedTest
   @NullAndEmptySource
   void testNoStepRunIdError(String stepId) {
      testIdStepRunError(stepId, "Step run must refer to the step by its id.");
   }

   @ParameterizedTest
   @ValueSource(strings = {"idWith space", "idWith.dot", "idWith{curlyBracket", "idWithCurlyBracket}",
         "idWith$", "idWith(bracket", "idWithBracket)"})
   void testInvalidStepRunIdError(String stepId) {
      testIdStepRunError(stepId, "Referred step id must not have chars '${}().' nor spaces.");
   }

   private void testIdStepRunError(String stepId, String expectedDescription) {
      TestRun testRun = new TestRun("Invalid step ids")
            .addStepRun(new StepRun().setId(stepId).setResult(StepResult.PASSED));
      new ValidationAdapter(testRun)
            .assertIsInvalid()
            .assertRuleFailure(expectedDescription);
   }

   @Test
   void testNoStepRunResultError() {
      TestRun testRun = new TestRun("Invalid step ids")
            .addStepRun(new StepRun().setId("MissingResultStepRun"));
      new ValidationAdapter(testRun)
            .assertIsInvalid()
            .assertRuleFailure("Step run must have a result.");
   }

   @Test
   void testValidTestRun() {
      TestRun testRun = new TestRun("Valid test")
            .addStepRuns(List.of(
                  new StepRun("step1", StepResult.DONE, new HashMap<>()),
                  new StepRun("step2", StepResult.PASSED, new HashMap<>()),
                  new StepRun("step3", StepResult.FAILED, new HashMap<>())
            ));
      new ValidationAdapter(testRun).assertIsValid();

      assertEquals("Valid test", testRun.getTestName(), "The test name must be accessible from the test run.");

      assertEquals(3, testRun.getStepRuns().size(), "All steps must be kept.");

      assertEquals("step1", testRun.getStepRun(0).getId(),
            "Step runs must be kept in the same order");
      assertEquals("step2", testRun.getStepRun(1).getId(),
            "Step runs must be kept in the same order");
      assertEquals("step3", testRun.getStepRun(2).getId(),
            "Step runs must be kept in the same order");

      assertEquals(testRun.getStepRun(0), testRun.getStepRun("step1"),
            "Step runs can be found by name");
      assertEquals(testRun.getStepRun(1), testRun.getStepRun("step2"),
            "Step runs can be found by name");
      assertEquals(testRun.getStepRun(2), testRun.getStepRun("step3"),
            "Step runs can be found by name");
   }

   @Test
   public void testGetTestResultDone() {
      TestRun testRun = new TestRun("test 2 steps");
      testRun.addStepRun(new StepRun("step1", StepResult.DONE, new HashMap<>()))
            .addStepRun(new StepRun("step2", StepResult.DONE, new HashMap<>()));
      assertEquals(TestResult.DONE, testRun.getResult(),
            "A test with no assert and no ERROR (i.e. only DONE steps) must have a DONE result");
   }

   @Test
   public void testGetTestResultPassed() {
      TestRun testRun = new TestRun("test 2 steps");
      testRun.addStepRuns(List.of(
            new StepRun("step1", StepResult.PASSED, new HashMap<>()),
            new StepRun("step2", StepResult.DONE, new HashMap<>())
      ));
      assertEquals(TestResult.PASSED, testRun.getResult(),
            "If at least one step has result PASSED but no ERROR and no FAILER, then the test result must be PASSED.");
   }

   @Test
   public void testGetTestResultFailed() {
      TestRun testRun = new TestRun("test 3 steps")
            .addStepRun(new StepRun("step1", StepResult.PASSED, new HashMap<>()))
            .addStepRun(new StepRun("step2", StepResult.DONE, new HashMap<>()))
            .addStepRun(new StepRun("step3", StepResult.FAILED, new HashMap<>()));
      assertEquals(TestResult.FAILED, testRun.getResult(),
            "If at least one step has result FAILED but no ERROR, then the test result must be FAILED.");
   }

   @Test
   public void testGetTestResultPairNumberOfFailed() {
      TestRun testRun = new TestRun("test 3 steps")
            .addStepRun(new StepRun("step1", StepResult.PASSED, new HashMap<>()))
            .addStepRun(new StepRun("step2", StepResult.FAILED, new HashMap<>()))
            .addStepRun(new StepRun("step3", StepResult.FAILED, new HashMap<>()));
      assertEquals(TestResult.FAILED, testRun.getResult(),
            "If at least one step has result FAILED but no ERROR, then the test result must be FAILED.");
   }

   @Test
   public void testGetTestResultError() {
      TestRun testRun = new TestRun("test 4 steps")
            .addStepRun(new StepRun("step1", StepResult.DONE, new HashMap<>()))
            .addStepRun(new StepRun("step2", StepResult.PASSED, new HashMap<>()))
            .addStepRun(new StepRun("step3", StepResult.FAILED, new HashMap<>()))
            .addStepRun(new StepRun("step4", StepResult.ERROR, new HashMap<>()));
      assertEquals(TestResult.ERROR, testRun.getResult(),
            "If at least one step has result ERROR, then the test result must be ERROR.");
   }

}
