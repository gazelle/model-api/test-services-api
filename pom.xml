<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>net.ihe.gazelle</groupId>
        <artifactId>parent-bom</artifactId>
        <version>0.0.1</version>
    </parent>

    <groupId>net.ihe.gazelle</groupId>
    <artifactId>test-services-api</artifactId>
    <version>0.0.2-SNAPSHOT</version>
    <packaging>pom</packaging>

    <name>Test Services API</name>

    <modules>
        <module>maestro-api</module>
        <module>messaging-api</module>
    </modules>

    <properties>
        <maven.compiler.source>11</maven.compiler.source>
        <maven.compiler.target>11</maven.compiler.target>

        <git.project.url>
            https://${git.user.name}:${git.user.token}@gitlab.inria.fr/gazelle/model-api/test-services-api.git
        </git.project.url>

        <surefire.version>3.0.0-M7</surefire.version>
        <junit.version>5.8.2</junit.version>
        <jacoco-maven-plugin.version>0.8.8</jacoco-maven-plugin.version>
        <framework.broker.version>0.0.1</framework.broker.version>
    </properties>

    <scm>
        <connection>scm:git:${git.project.url}</connection>
        <developerConnection>scm:git:${git.project.url}</developerConnection>
        <url>https://gitlab.inria.fr/gazelle/model-api/test-services-api</url>
        <tag>HEAD</tag>
    </scm>

    <build>
        <plugins>
            <plugin>
                <!-- Run unit-test in maven build -->
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>${surefire.version}</version>
                <configuration>
                    <skip>${skipUnitTests}</skip>
                </configuration>
                <dependencies>
                    <dependency>
                        <groupId>org.junit.jupiter</groupId>
                        <artifactId>junit-jupiter-engine</artifactId>
                        <version>${junit.version}</version>
                    </dependency>
                </dependencies>
            </plugin>
            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <version>${jacoco-maven-plugin.version}</version>
                <executions>
                    <execution>
                        <id>unit-prepare-agent</id>
                        <goals>
                            <goal>prepare-agent</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>unit-report</id>
                        <phase>test</phase>
                        <goals>
                            <goal>report</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <version>3.3.1</version>
                <executions>
                    <execution>
                        <id>generate-javadoc</id>
                        <phase>generate-resources</phase>
                        <goals>
                            <goal>javadoc</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>package-javadoc</id>
                        <phase>package</phase>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.sonarsource.scanner.maven</groupId>
                <artifactId>sonar-maven-plugin</artifactId>
                <version>3.7.0.1746</version>
            </plugin>
        </plugins>
    </build>

    <dependencyManagement>
        <dependencies>
            <!-- sub-module management for inter-dependencies -->
            <dependency>
                <groupId>net.ihe.gazelle</groupId>
                <artifactId>maestro-api</artifactId>
                <version>0.0.2-SNAPSHOT</version>
            </dependency>
            <dependency>
                <groupId>net.ihe.gazelle</groupId>
                <artifactId>messaging-api</artifactId>
                <version>0.0.2-SNAPSHOT</version>
                <scope>provided</scope>
            </dependency>

            <!-- Gazelle dependencies  -->
            <dependency>
                <groupId>net.ihe.gazelle</groupId>
                <artifactId>lang</artifactId>
                <version>0.0.1</version>
            </dependency>


            <!-- Other Provided dependencies -->
            <dependency>
                <groupId>org.apache.kafka</groupId>
                <artifactId>kafka-clients</artifactId>
                <version>3.1.0</version>
                <scope>provided</scope>
            </dependency>
            <dependency>
                <groupId>com.fasterxml.jackson.core</groupId>
                <artifactId>jackson-databind</artifactId>
                <version>2.13.0</version>
                <scope>provided</scope>
            </dependency>

            <!-- test dependencies -->
            <dependency>
                <groupId>org.junit.jupiter</groupId>
                <artifactId>junit-jupiter-api</artifactId>
                <version>${junit.version}</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>org.junit.jupiter</groupId>
                <artifactId>junit-jupiter-params</artifactId>
                <version>${junit.version}</version>
                <scope>test</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <repositories>
        <repository>
            <id>parent-bom-repository</id>
            <url>https://gazelle.ihe.net/nexus/content/groups/public/</url>
        </repository>
    </repositories>
</project>

