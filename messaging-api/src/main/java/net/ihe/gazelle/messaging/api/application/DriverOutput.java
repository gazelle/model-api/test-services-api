package net.ihe.gazelle.messaging.api.application;

public class DriverOutput<P extends Response<?>> {

   private P response;
   // FIXME: 19/08/2022 Throwable cannot be serialized due to its inner vector.
   private Throwable exception;

   public DriverOutput() {
   }

   public DriverOutput(P response) {
      this.response = response;
   }

   public DriverOutput(Throwable exception) {
      this.exception = exception;
   }

   public P getResponse() {
      return response;
   }

   public DriverOutput<P> setResponse(P response) {
      this.response = response;
      return this;
   }

   public Throwable getException() {
      return exception;
   }

   public DriverOutput<P> setException(Throwable exception) {
      this.exception = exception;
      return this;
   }

   public boolean hasError() {
      return exception != null;
   }
}
