package net.ihe.gazelle.messaging.api.interlay;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.ihe.gazelle.messaging.api.application.http.RespondedCommand;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Serializer;

public class RespondedCommandSerializer implements Serializer<RespondedCommand> {

    @Override
    public byte[] serialize(String topic, RespondedCommand respondedCommand) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsBytes(respondedCommand);
        } catch (JsonProcessingException e) {
            throw new SerializationException(e);
        }
    }
}
