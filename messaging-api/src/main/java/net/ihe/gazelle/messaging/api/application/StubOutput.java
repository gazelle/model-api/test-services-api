package net.ihe.gazelle.messaging.api.application;

public class StubOutput<R extends Request> {
   private R request;
   private Throwable exception;

   public StubOutput() {
   }

   public StubOutput(R request) {
      this.request = request;
   }

   public StubOutput(Throwable exception) {
      this.exception = exception;
   }

   public R getRequest() {
      return request;
   }

   public StubOutput<R> setRequest(R request) {
      this.request = request;
      return this;
   }

   public Throwable getException() {
      return exception;
   }

   public StubOutput<R> setException(Throwable exception) {
      this.exception = exception;
      return this;
   }

   public boolean hasError() {
      return exception != null;
   }
}
