package net.ihe.gazelle.messaging.api.interlay;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.ihe.gazelle.messaging.api.application.http.InitiateCommand;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Serializer;

public class InitiateCommandSerializer implements Serializer<InitiateCommand> {

    @Override
    public byte[] serialize(String topic, InitiateCommand initiateCommand) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsBytes(initiateCommand);
        } catch (JsonProcessingException e) {
            throw new SerializationException(e);
        }
    }
}
