package net.ihe.gazelle.messaging.api.interlay;

import net.ihe.gazelle.lang.ReflectionOperations;

public interface MessagingTopics {

    public static final String TEST_DRIVER = "test-driver";

    public static final String[] TOPICS = (new ReflectionOperations())
            .getStringConstantValuesReflection(MessagingTopics.class);


}
