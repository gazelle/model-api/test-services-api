package net.ihe.gazelle.messaging.api.application.http;

import java.net.http.HttpRequest;

public interface HTTPCaller {

    /**
     * Attach a HTTP Request to the caller.
     *
     * @param httpRequest The HTTP request to build
     * @return a java HTTP Request ready to be used.
     */
    HttpRequest setHTTPRequest(HTTPRequest httpRequest);

    /**
     * Execute the built HTTP request.
     *
     * @return the corresponding HTTP response.
     */
    HTTPResponse callHTTPEndpoint();
}
