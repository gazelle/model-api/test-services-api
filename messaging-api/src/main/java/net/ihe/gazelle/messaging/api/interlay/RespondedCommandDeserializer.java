package net.ihe.gazelle.messaging.api.interlay;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.ihe.gazelle.messaging.api.application.http.RespondedCommand;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Deserializer;

import java.io.IOException;

public class RespondedCommandDeserializer implements Deserializer<RespondedCommand> {

    @Override
    public RespondedCommand deserialize(String topic, byte[] bytes) {
        try{
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(bytes, RespondedCommand.class);
        } catch (IOException e) {
            throw new SerializationException(e);
         }
    }
}
