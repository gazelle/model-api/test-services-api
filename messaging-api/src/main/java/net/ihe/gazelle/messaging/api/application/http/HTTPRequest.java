package net.ihe.gazelle.messaging.api.application.http;

import net.ihe.gazelle.messaging.api.application.Address;
import net.ihe.gazelle.messaging.api.application.Request;

import java.util.HashMap;
import java.util.Map;

public class HTTPRequest extends Request {

    private String resource;

    private HTTPMethod method;

    private String httpVersion;

    private final Map<String, String> headers = new HashMap<>();

    private final Map<String, String> queryParams = new HashMap<>();

    private byte[] body;

    boolean secure = false;

    public HTTPRequest() {}

    public HTTPRequest(Address destination, String resource, HTTPMethod method) {
        super(destination);
        this.resource = resource;
        this.method = method;
    }

    public HTTPRequest(Address destination, String resource, HTTPMethod method, byte[] body) {
        super(destination);
        this.resource = resource;
        this.method = method;
        this.body = body;
    }

    public String getResource() {
        return resource;
    }

    public HTTPRequest setResource(String resource) {
        this.resource = resource;
        return this;
    }

    public HTTPMethod getMethod() {
        return method;
    }

    public HTTPRequest setMethod(HTTPMethod method) {
        this.method = method;
        return this;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public Map<String, String> getQueryParams() {
        return queryParams;
    }

    public byte[] getBody() {
        return body;
    }

    public HTTPRequest setBody(byte[] body) {
        this.body = body;
        return this;
    }

    public String getHttpVersion() {
        return httpVersion;
    }

    public HTTPRequest setHttpVersion(String httpVersion) {
        this.httpVersion = httpVersion;
        return this;
    }

    public boolean isSecure() {
        return secure;
    }

    public HTTPRequest setSecure(boolean secure) {
        this.secure = secure;
        return this;
    }
}
