package net.ihe.gazelle.messaging.api.interlay;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.ihe.gazelle.messaging.api.application.http.InitiateCommand;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Deserializer;

import java.io.IOException;

public class InitiateCommandDeserializer implements Deserializer<InitiateCommand> {

    @Override
    public InitiateCommand deserialize(String topic, byte[] bytes) {
        try{
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(bytes, InitiateCommand.class);
        } catch (IOException e) {
            throw new SerializationException(e);
         }
    }
}
