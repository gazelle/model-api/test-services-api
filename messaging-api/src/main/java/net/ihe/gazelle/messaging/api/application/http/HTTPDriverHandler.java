package net.ihe.gazelle.messaging.api.application.http;

import net.ihe.gazelle.messaging.api.application.DriverHandler;

public interface HTTPDriverHandler extends DriverHandler<HTTPRequest, HTTPResponse> {


}
