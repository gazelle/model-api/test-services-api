package net.ihe.gazelle.messaging.api.application.tcp;

import net.ihe.gazelle.messaging.api.application.Response;

public class TCPResponse extends Response<TCPRequest> {

   private byte[] message ;

   public TCPResponse() {
   }

   public TCPResponse(TCPRequest request, byte[] message) {
      super(request);
      this.message = message;
   }

   public byte[] getMessage() {
      return message;
   }

   public TCPResponse setMessage(byte[] message) {
      this.message = message;
      return this;
   }
}
