package net.ihe.gazelle.messaging.api.application.http;

import net.ihe.gazelle.lang.AssertableModel;
import net.ihe.gazelle.lang.ModelAssertionException;
import net.ihe.gazelle.messaging.api.application.DriverOutput;

import static net.ihe.gazelle.lang.ModelAsserts.assertIsDefined;

public class RespondedCommand  implements AssertableModel {

    private DriverOutput<HTTPResponse> response;
    private String callbackId;

    public RespondedCommand() {
        // Required to deserialize data with reactive messaging.
    }


    public RespondedCommand(DriverOutput<HTTPResponse> response, String callbackId) {
        this.response = response;
        this.callbackId = callbackId;
    }

    public String getCallbackId() {
        return callbackId;
    }

    public DriverOutput<HTTPResponse> getResponse() {
        return response;
    }

    @Override
    public void assertIsValid() {
        if (getResponse() == null)
            throw new ModelAssertionException(" HttpResponse can't be null.");
        assertIsDefined(callbackId,"RespondedCommand.callbackId");
        assertIsDefined(response.getResponse(), "RespondedCommand.response.response");
        assertIsDefined(response.getResponse().getSource(),"RespondedCommand.response.source");
        assertIsDefined(response.getResponse().getStatus(),"RespondedCommand.response.status");
    }
}
