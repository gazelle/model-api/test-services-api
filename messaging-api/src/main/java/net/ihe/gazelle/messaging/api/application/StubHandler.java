package net.ihe.gazelle.messaging.api.application;

import net.ihe.gazelle.maestro.api.application.Handler;

import java.util.function.Consumer;
import java.util.function.Function;

public interface StubHandler<R extends Request, P extends Response<R>> extends Handler {

   void respond(Consumer<String> readyToAcceptCallback, Function<R, P> responseSupplier, StubParams stubParams,
                Consumer<StubOutput<R>> callback);

}
