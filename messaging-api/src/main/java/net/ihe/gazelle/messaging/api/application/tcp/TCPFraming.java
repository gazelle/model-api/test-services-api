package net.ihe.gazelle.messaging.api.application.tcp;

/**
 * Define the boundaries of application-messages exchanged in a TCP client-server connection. TCP controllers need such
 * scheme to distinguish application-messages from the other in a single byte-stream.
 */
public interface TCPFraming {

   /**
    * TCP Counting framing is when messages are split by their length (the lenght information is appended before the
    * message payload).
    */
   public static class Counting implements TCPFraming {

      private final byte lengthDelimiter;

      /**
       * Instantiate a TCP Counting framing.
       *
       * @param lengthDelimiter expected byte to signify the end of the length information. The parser needs it to
       *                        identify when to switch to parsing the payload.
       */
      public Counting(byte lengthDelimiter) {
         this.lengthDelimiter = lengthDelimiter;
      }

      public byte getLengthDelimiter() {
         return lengthDelimiter;
      }

   }

   /**
    * Non-transparent framing is when several messages are separated with a special byte delimiter.
    */
   public static class NonTransparent implements TCPFraming {

      private final byte delimiter;

      /**
       * Instantiate a TCP Non-Transparent framing.
       *
       * @param delimiter expected byte to identify the end of the message payload.
       */
      public NonTransparent(byte delimiter) {
         this.delimiter = delimiter;
      }

      public byte getDelimiter() {
         return delimiter;
      }

   }

}
