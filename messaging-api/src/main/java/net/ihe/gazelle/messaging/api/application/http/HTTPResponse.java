package net.ihe.gazelle.messaging.api.application.http;

import net.ihe.gazelle.messaging.api.application.Response;

import java.util.HashMap;
import java.util.Map;

public class HTTPResponse extends Response<HTTPRequest> {

    private int status;

    private final Map<String, String> headers = new HashMap<>();

    public HTTPResponse() {}

    public HTTPResponse(HTTPRequest initialRequest, int status) {
        super(initialRequest);
        this.status = status;
    }

    public HTTPResponse(HTTPRequest initialRequest, int status, byte[] body) {
        super(initialRequest);
        this.status = status;
        this.body = body;
    }

    private byte[] body;

    public int getStatus() {
        return status;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public byte[] getBody() {
        return body;
    }

    public HTTPResponse setStatus(int status) {
        this.status = status;
        return this;
    }

    public HTTPResponse setBody(byte[] body) {
        this.body = body;
        return this;
    }
}
