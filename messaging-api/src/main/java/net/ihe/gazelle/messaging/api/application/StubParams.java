package net.ihe.gazelle.messaging.api.application;

public class StubParams {

   private String acceptFromHost;
   private int timeout;

   public StubParams() {
      // Required for deserialization with reactive messaging.
   }

   public String getAcceptFromHost() {
      return acceptFromHost;
   }

   public StubParams setAcceptFromHost(String acceptFromHost) {
      this.acceptFromHost = acceptFromHost;
      return this;
   }

   public int getTimeout() {
      return timeout;
   }

   public StubParams setTimeout(int timeout) {
      this.timeout = timeout;
      return this;
   }
}
