package net.ihe.gazelle.messaging.api.application.http;


import net.ihe.gazelle.lang.AssertableModel;
import net.ihe.gazelle.lang.ModelAssertionException;

import static net.ihe.gazelle.lang.ModelAsserts.assertIsDefined;

public class InitiateCommand implements AssertableModel {

    private HTTPRequest httpRequest;
    private String callbackId;

    public  InitiateCommand() {
        // Required to deserialize data with reactive messaging.
    }

    public InitiateCommand(HTTPRequest httpRequest, String callbackId) {
        this.httpRequest = httpRequest;
        this.callbackId = callbackId;
    }
    public HTTPRequest getHttpRequest() {
        return httpRequest;
    }

    public InitiateCommand setHttpRequest(HTTPRequest httpRequest) {
        this.httpRequest = httpRequest;
        return this;
    }

    public String getCallbackId() {
        return callbackId;
    }

    public InitiateCommand setCallbackId(String callbackId) {
        this.callbackId = callbackId;
        return this;
    }

    @Override
    public void assertIsValid() {
        if (getHttpRequest() == null)
            throw new ModelAssertionException(" HttpRequest can't be null.");
        assertIsDefined(getCallbackId(),"initiateCommand.callBackId");
        assertIsDefined(getHttpRequest().getDestination(),"initiateCommand.httpRequest.destination");
        assertIsDefined(getHttpRequest().getMethod(),"initiateCommand.httpRequest.method");
    }
}
