package net.ihe.gazelle.messaging.api.application.tcp;

import net.ihe.gazelle.messaging.api.application.Address;
import net.ihe.gazelle.messaging.api.application.Request;

public class TCPRequest extends Request {

   private byte[] message;
   private TCPFraming framing;

   public TCPRequest() {
   }

   public TCPRequest(byte[] message) {
      this.message = message;
   }

   public TCPRequest(Address destination, TCPFraming framing, byte[] message) {
      super(destination);
      this.message = message;
      this.framing = framing;
   }

   public byte[] getMessage() {
      return message;
   }

   public TCPRequest setMessage(byte[] message) {
      this.message = message;
      return this;
   }

   public TCPFraming getFraming() {
      return framing;
   }

   public TCPRequest setFraming(TCPFraming framing) {
      this.framing = framing;
      return this;
   }
}
