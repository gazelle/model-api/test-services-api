package net.ihe.gazelle.messaging.api.application;

import java.util.function.Consumer;

public interface DriverCallback<T extends Response<?>> extends Consumer<DriverOutput<T>> {

}
