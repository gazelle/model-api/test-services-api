package net.ihe.gazelle.messaging.api.application.tcp;

import net.ihe.gazelle.messaging.api.application.StubHandler;

public interface TCPStubHandler extends StubHandler<TCPRequest, TCPResponse> {

}
