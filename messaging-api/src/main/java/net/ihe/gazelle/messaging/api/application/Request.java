package net.ihe.gazelle.messaging.api.application;

public abstract class Request {

   private Address source;
   private Address destination;

   public Request() {
   }

   public Request(Address destination) {
      this.destination = destination;
   }

   public Address getSource() {
      return source;
   }

   public Request setSource(Address source) {
      this.source = source;
      return this;
   }

   public Address getDestination() {
      return destination;
   }

   public Request setDestination(Address destination) {
      this.destination = destination;
      return this;
   }
}
