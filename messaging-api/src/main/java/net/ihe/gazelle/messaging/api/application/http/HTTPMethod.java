package net.ihe.gazelle.messaging.api.application.http;

import java.util.Arrays;

public enum HTTPMethod {
    GET("get"),
    POST("post"),
    PUT("put"),
    DELETE("delete");

    private String value;

    HTTPMethod(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static HTTPMethod of(String method){
        return Arrays.stream(HTTPMethod.values())
                .filter(httpMethod->httpMethod.value.equalsIgnoreCase(method))
                .findFirst()
                .orElse(null);
    }

}

