package net.ihe.gazelle.messaging.api.application.tcp;

import net.ihe.gazelle.messaging.api.application.DriverHandler;

public interface TCPDriverHandler extends DriverHandler<TCPRequest, TCPResponse> {

}
