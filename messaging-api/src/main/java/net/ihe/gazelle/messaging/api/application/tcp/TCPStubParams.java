package net.ihe.gazelle.messaging.api.application.tcp;

import net.ihe.gazelle.messaging.api.application.StubParams;

public class TCPStubParams extends StubParams {

   private Integer bindingPort ;
   private TCPFraming tcpFraming;

   public TCPStubParams() {
      // Required for deserialization with reactive messaging.
   }

   public Integer getBindingPort() {
      return bindingPort;
   }

   public TCPStubParams setBindingPort(Integer bindingPort) {
      this.bindingPort = bindingPort;
      return this;
   }

   public TCPFraming getTcpFraming() {
      return tcpFraming;
   }

   public TCPStubParams setTcpFraming(TCPFraming tcpFraming) {
      this.tcpFraming = tcpFraming;
      return this;
   }
}
