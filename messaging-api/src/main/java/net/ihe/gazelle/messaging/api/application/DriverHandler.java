package net.ihe.gazelle.messaging.api.application;

import net.ihe.gazelle.maestro.api.application.Handler;

import java.util.function.Consumer;

public interface DriverHandler<R extends Request, P extends Response<R>> extends Handler {

   void initiate(R request, Consumer<DriverOutput<P>> callback);

}
