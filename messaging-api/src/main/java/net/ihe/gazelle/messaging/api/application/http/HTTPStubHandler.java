package net.ihe.gazelle.messaging.api.application.http;

import net.ihe.gazelle.messaging.api.application.StubHandler;

public interface HTTPStubHandler extends StubHandler<HTTPRequest, HTTPResponse> {
}
