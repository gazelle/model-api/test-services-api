package net.ihe.gazelle.messaging.api.application;

public abstract class Response<R extends Request> {

   private R request ;
   private Address source;
   private Address destination;

   public Response() {
   }

   public Response(R initialRequest) {
      this.source = initialRequest.getDestination();
      this.request = initialRequest;
   }

   public Address getSource() {
      return source;
   }

   public Response<R> setSource(Address source) {
      this.source = source;
      return this;
   }

   public Address getDestination() {
      return destination;
   }

   public Response<R> setDestination(Address destination) {
      this.destination = destination;
      return this;
   }

   public R getRequest() {
      return request;
   }

   public Response<R> setRequest(R request) {
      this.request = request;
      return this;
   }
}
