package net.ihe.gazelle.messaging.api.application;

import net.ihe.gazelle.lang.ModelAssertionException;
import net.ihe.gazelle.messaging.api.application.http.HTTPMethod;
import net.ihe.gazelle.messaging.api.application.http.HTTPRequest;
import net.ihe.gazelle.messaging.api.application.http.HTTPResponse;
import net.ihe.gazelle.messaging.api.application.http.RespondedCommand;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

class RespondedCommandTest {

    @ParameterizedTest
    @MethodSource("provideWrongRespondedCommand")
    void testWrongTransactionInitiate(RespondedCommand respondedCommand, Class<? extends Exception> expectedExceptionClass) {
        Assertions.assertThrows(expectedExceptionClass, respondedCommand::assertIsValid);
    }

    @Test
    void testCorrectRespondedCommand() {
        Address address = new Address("source",1);
        HTTPRequest httpRequest = new HTTPRequest(address,"resource", HTTPMethod.GET);
        httpRequest.setSource(address);
        httpRequest.setHttpVersion("HTTP_2");
        HTTPResponse httpResponse = new HTTPResponse(httpRequest,200,new byte[1]);
        httpResponse.setDestination(new Address("destination",2));
        RespondedCommand respondedCommand = new RespondedCommand(new DriverOutput<>(httpResponse),"555");

        Assertions.assertDoesNotThrow(respondedCommand::assertIsValid);
        Assertions.assertEquals("555", respondedCommand.getCallbackId());
        Assertions.assertNotNull(respondedCommand.getResponse());
        Assertions.assertEquals("source",respondedCommand.getResponse().getResponse().getSource().getHost());
        Assertions.assertEquals(1,respondedCommand.getResponse().getResponse().getSource().getPort());
        Assertions.assertEquals("destination",respondedCommand.getResponse().getResponse().getDestination().getHost());
        Assertions.assertEquals(2,respondedCommand.getResponse().getResponse().getDestination().getPort());
    }

    // TODO: 10/08/2022 Test to be reviewed
    @Test
    void testMissingSourceRespondedCommand() {
        Address address = new Address("localhost",8080);
        HTTPRequest httpRequest = new HTTPRequest(address,"resource", HTTPMethod.GET);
        httpRequest.setSource(address);
        httpRequest.setHttpVersion("HTTP_2");
        HTTPResponse httpResponse = new HTTPResponse(httpRequest,200,new byte[1]);
        RespondedCommand respondedCommand = new RespondedCommand(new DriverOutput<>(httpResponse),"555");

        Assertions.assertEquals("555", respondedCommand.getCallbackId());
        Assertions.assertNull(respondedCommand.getResponse().getResponse().getDestination());
        Assertions.assertNotNull(respondedCommand.getResponse().getResponse().getSource());
    }

    static Stream<Arguments> provideWrongRespondedCommand() {
        HTTPRequest basicHTTPRequest = new HTTPRequest(new Address("  ",443),"rest/services",HTTPMethod.GET);
        basicHTTPRequest.setSource(new Address("source.localhost",80));
        basicHTTPRequest.setHttpVersion("HTTP_2");
        DriverOutput<HTTPResponse> httpResponse = new DriverOutput<>(new HTTPResponse(basicHTTPRequest,200,new byte[1]));
        httpResponse.getResponse().setDestination(new Address("destination",443));

        return Stream.of(
                Arguments.of(new RespondedCommand(), ModelAssertionException.class),
                Arguments.of(new RespondedCommand(null,null), ModelAssertionException.class),
                Arguments.of(new RespondedCommand(httpResponse,null), ModelAssertionException.class),
                Arguments.of(new RespondedCommand(null,"555"), ModelAssertionException.class),
                Arguments.of(new RespondedCommand(new DriverOutput<>((HTTPResponse) httpResponse.getResponse().setSource(null)),"555"), ModelAssertionException.class),
                Arguments.of(new RespondedCommand(new DriverOutput<>((HTTPResponse) httpResponse.getResponse().setDestination(null)),"555"), ModelAssertionException.class)
        );
    }
}
