package net.ihe.gazelle.messaging.api.application;

import net.ihe.gazelle.lang.ModelAssertionException;
import net.ihe.gazelle.messaging.api.application.http.HTTPMethod;
import net.ihe.gazelle.messaging.api.application.http.HTTPRequest;
import net.ihe.gazelle.messaging.api.application.http.InitiateCommand;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

class InitiateCommandTest {


    @ParameterizedTest
    @MethodSource("provideWrongInitiateCommand")
    void testWrongTransactionInitiate(InitiateCommand initiateCommand, Class<? extends Exception> expectedExceptionClass) {
        Assertions.assertThrows(expectedExceptionClass, initiateCommand::assertIsValid);
    }

    @Test
    void testCorrectTransactionInitiate() {
        Address address = new Address("localhost",8080);
        HTTPRequest httpRequest = new HTTPRequest(address,"resource", HTTPMethod.GET);
        httpRequest.setSource(address);
        httpRequest.setHttpVersion("HTTP_2");
        InitiateCommand initiateCommand = new InitiateCommand(httpRequest,"555");
        Assertions.assertDoesNotThrow(initiateCommand::assertIsValid);
        Assertions.assertEquals("555", initiateCommand.getCallbackId());
    }

    static Stream<Arguments> provideWrongInitiateCommand() {

        return Stream.of(
                Arguments.of(new InitiateCommand(), ModelAssertionException.class),
                Arguments.of(new InitiateCommand(null,null), ModelAssertionException.class),
                Arguments.of(new InitiateCommand(new HTTPRequest(null,null,null),"555"), ModelAssertionException.class),
                Arguments.of(new InitiateCommand(basicHTTPRequest().setMethod(null).setHttpVersion("HTTP_2"),"555"), ModelAssertionException.class),
                Arguments.of(new InitiateCommand(basicHTTPRequest().setMethod(HTTPMethod.GET).setHttpVersion("HTTP_2"),"555").setHttpRequest(null), ModelAssertionException.class),
                Arguments.of(new InitiateCommand(basicHTTPRequest().setMethod(HTTPMethod.GET).setHttpVersion("HTTP_2"),"555").setCallbackId(null), ModelAssertionException.class)
        );
    }

    static HTTPRequest basicHTTPRequest() {
        HTTPRequest basicHTTPRequest = new HTTPRequest(new Address("localhost",443),"rest/services",HTTPMethod.GET);
        basicHTTPRequest.setSource(new Address("source.localhost",80));
        basicHTTPRequest.setHttpVersion("HTTP_2");
        return basicHTTPRequest;
    }

}
